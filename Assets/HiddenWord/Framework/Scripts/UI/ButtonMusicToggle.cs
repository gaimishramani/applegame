﻿using UnityEngine;
using System.Collections;
namespace dotmob
{

    public class ButtonMusicToggle : TButton
    {

        protected override void Start()
        {
            base.Start();
            //IsOn = Music.instance.IsEnabled();
        }

        public override void OnButtonClick()
        {
            base.OnButtonClick();
            //Music.instance.SetEnabled(IsOn, true);
            SoundManager.Instance.SetSoundTypeOnOff(SoundManager.SoundType.Music, isOn);
        }
    }
}
