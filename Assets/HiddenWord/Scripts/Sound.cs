using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(fileName = "SoundDataSo", menuName = "ScriptableObjects/SoundDataSo", order = 1)]

public class Sound : ScriptableObject
{
    public SoundData[] soundClips;
}

[System.Serializable]
public class SoundData
{
    public string soundName;
    public SoundType soundType;
    public AudioClip clip;
    public bool isLoop = true;

    [Range(0f, 1f)]
    public float volume = 1;
    [Range(0f, 1f)]
    public float pitch = 1;
    
    

    [HideInInspector]
    public AudioSource audioSource;
}
public enum SoundType
{
    Music,
    Sound,
    None,
    Vibration
}