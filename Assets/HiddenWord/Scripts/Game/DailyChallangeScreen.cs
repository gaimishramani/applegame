﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

namespace dotmob.HiddenWord
{
    public class DailyChallangeScreen : Screen
    {
        #region Inspector Variables
        public TextMeshProUGUI completedLelelsText;
        public TextMeshProUGUI lastDateOfMonthText;
        public Image point2;
        public Image point3;
        public Image point4;
        public Image levelProgressBar;

        public Image trophyImage;
        /*[Space]

		[SerializeField] private LevelListItem	levelListItemPrefab	= null;
		[SerializeField] private RectTransform	levelListContainer	= null;
		[SerializeField] private ScrollRect		levelListScrollRect	= null;
*/
        #endregion

        #region Member Variables

        //private RecyclableListHandler<int> levelListHandler;

        #endregion

        #region Public Methods

        public override void Show(bool back, bool immediate)
        {
            base.Show(back, immediate);
            ScreenManager.Instance.topBar.GetComponent<TopBar>().OnChallengeScreen();
        }

        #endregion

        #region Private Methods

        public void SetProgressBarData()
        {
            String key = CalendarManager.Instance.currentMonthInWord + CalendarManager.Instance.currentYear.ToString();
            Debug.Log("key " + key);
            string completedLevel = "Completed" + PlayerPrefs.GetString(key);
            Debug.Log("comple " + PlayerPrefs.GetInt(completedLevel).ToString());
            completedLelelsText.text = PlayerPrefs.GetInt(completedLevel).ToString() + "/" + CalendarManager.Instance.endDay;
            lastDateOfMonthText.text = CalendarManager.Instance.endDay.ToString();
            levelProgressBar.fillAmount = (1 / (float)CalendarManager.Instance.endDay) * (float)PlayerPrefs.GetInt(completedLevel);
        }

        public void OnLevelBttonClicked()
        {
            ScreenManager.Instance.Show("levels");
        }

        public void OnDailyChallangeClicked()
        {
            ScreenManager.Instance.Show("dailychallenge");
        }
        public void GenerateDailyChallange()
        {
            int generateCategory = GetDailyChallangeCategory();
            int generateLevel = GetDailyChallangeLevel(generateCategory);
            GameManager.Instance.ActiveCategoryInfo = GameManager.Instance.CategoryInfos[generateCategory];
            GameManager.Instance.StartLevel(GameManager.Instance.ActiveCategoryInfo, generateLevel);
            GameManager.Instance.gameType = GameType.DAILY_CHALLANGE;
            Debug.Log("gene " + GameManager.Instance.CategoryInfos[generateCategory]);
            Debug.Log("gene in " + generateLevel);
            SoundManager.Instance.Play("btn-click");
            GameManager.Instance.topBar.titleText.text = "Daily";

        }

        private int GetDailyChallangeCategory()
        {
            return UnityEngine.Random.Range(0, GameManager.Instance.CategoryInfos.Count);
        }
        private int GetDailyChallangeLevel(int dailyChallangeCategoryIndex)
        {
            return UnityEngine.Random.Range(0, GameManager.Instance.CategoryInfos[dailyChallangeCategoryIndex].levelFiles.Count);
        }
        public void OnDailyChallangeLevelCompleted()
        {

        }
        #endregion
    }
}
