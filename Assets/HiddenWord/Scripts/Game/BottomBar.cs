﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace dotmob.HiddenWord
{
	public class BottomBar : MonoBehaviour
	{
		#region Inspector Variables

		public Image levelButton;
		public Image dailyChallangeButton;
		

		#endregion

		#region Member Variables

		#endregion

		#region Properties

		#endregion

		#region Unity Methods

		private void Update()
		{
			
		}

		#endregion

		#region Public Methods

		private void Start()
		{
		}

		#endregion

		#region Protected Methods

		#endregion

		#region Private Methods

		
		private void FadeIn(CanvasGroup canvasGroup)
		{
			UIAnimation anim = UIAnimation.Alpha(canvasGroup, 0f, 1f, 0.5f);
			anim.style = UIAnimation.Style.EaseOut;
			anim.Play();
		}

		private void FadeOut(CanvasGroup canvasGroup)
		{
			UIAnimation anim = UIAnimation.Alpha(canvasGroup, 1f, 0f, 0.5f);
			anim.style = UIAnimation.Style.EaseOut;
			anim.Play();
		}

		
		#endregion
	}
}
