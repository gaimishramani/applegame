﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace dotmob.HiddenWord
{
	public class GameScreen : Screen
	{
		#region Inspector Variables

		[Space]

		[SerializeField] private Text wordHintCostText;
		[SerializeField] private Text letterHintCostText;

		#endregion

		#region Public Methods

		public override void Initialize()
		{
			base.Initialize();

			/*wordHintCostText.text = "x" + GameManager.Instance.CoinCostWordHint;
			letterHintCostText.text = "x" + GameManager.Instance.CoinCostLetterHint;*/
		}

		#endregion
	}
}
