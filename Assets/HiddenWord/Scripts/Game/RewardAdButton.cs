﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace dotmob.HiddenWord
{
    [RequireComponent(typeof(Button))]
    public class RewardAdButton : MonoBehaviour
    {
        #region Inspector Variables

        [SerializeField] private int coinsToReward;

        #endregion

        #region Properties

        public Button Button { get { return gameObject.GetComponent<Button>(); } }

        #endregion

        #region Unity Methods

        private void Awake()
        {
            Button.onClick.AddListener(OnClick);
        }

        #endregion

        #region Private Methods

        private void OnClick()
        {
            PopupManager.Instance.CloseActivePopup();
            Debug.Log("reward ad load");
        }

        private void OnRewardAdLoaded()
        {
            gameObject.SetActive(true);
        }

        private void OnRewardAdClosed()
        {

        }

        private void OnRewardAdGranted(string rewardId, double rewardAmount)
        {
            GameManager.Instance.GiveCoins(coinsToReward);
            SoundManager.Instance.Play("coin-collect");
            PopupManager.Instance.rewardAdPopup.Hide(true);
            // Show the popup to the user so they know they got the hint
        }

        private void OnAdsRemoved()
        {
        }

        #endregion
    }
}
