﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace dotmob.HiddenWord
{
    public class CategoryListItem : RecyclableListItem<CategoryInfo>
    {
        #region Inspector Variables

        [SerializeField] private Text nameText = null;
        [SerializeField] private Image iconImage = null;
        [SerializeField] private Image backgroundImage = null;
        [SerializeField] private ProgressBar levelProgressBar = null;
        [SerializeField] private Text levelProgressText = null;

        [Space]

        [SerializeField] private GameObject progressBarContainer = null;
        [SerializeField] private GameObject lockedContainer = null;
        [SerializeField] private GameObject coinsUnlockContainer = null;
        [SerializeField] private GameObject keysUnlockContainer = null;
        [SerializeField] private GameObject iapUnlockContainer = null;

        [Space]

        [SerializeField] private Text coinsUnlockAmountText = null;
        [SerializeField] private Text keysUnlockAmountText = null;
        [SerializeField] private Text iapUnlockPriceText = null;

        public Image borderImage;

        #endregion

        #region Member Variables

        private const float animDuration = 400f;
        private const float animStartY = -150f;
        private const float animStartScale = 0.8f;

#if BBG_IAP
		private bool	waitingForIAPInitialize;
		private string	lockedProductId;
#endif

        #endregion

        #region Public Methods
        /* private void Start()
         {
             if (GameManager.Instance.ActiveCategoryInfo.displayName == nameText.ToString())
                 borderImage.color = GameManager.Instance.selectedColor;
         }*/
        public override void Initialize(CategoryInfo categoryInfo)
        {

        }
        private void Update()
        {
            /*if (GameManager.Instance.ActiveCategoryInfo.displayName == nameText.text)
                borderImage.color = GameManager.Instance.selectedColor;
            else
                borderImage.color = GameManager.Instance.originalColor;*/
        }
        /*public void OnColorChange()
        {
            if (GameManager.Instance.ActiveCategoryInfo.displayName == nameText.text)
                borderImage.color = GameManager.Instance.selectedColor;
        }*/
        public override void Setup(CategoryInfo categoryInfo)
        {
            nameText.text = categoryInfo.displayName;
            iconImage.sprite = categoryInfo.icon;
            
            if (categoryInfo.colorType == CategoryInfo.ColorType.PINK)
            {
                backgroundImage.sprite = GameManager.Instance.categoryInfoSpries[0];
                nameText.color = GameManager.Instance.categoryInfosColors[0];
            }
            else if (categoryInfo.colorType == CategoryInfo.ColorType.GREEN)
            {
                backgroundImage.sprite = GameManager.Instance.categoryInfoSpries[1];
                nameText.color = GameManager.Instance.categoryInfosColors[1];
            }
            else if (categoryInfo.colorType == CategoryInfo.ColorType.CYAN)
            {
                backgroundImage.sprite = GameManager.Instance.categoryInfoSpries[2];
                nameText.color = GameManager.Instance.categoryInfosColors[2];
            }
            else if (categoryInfo.colorType == CategoryInfo.ColorType.ORANGE)
            {
                backgroundImage.sprite = GameManager.Instance.categoryInfoSpries[3];
                nameText.color = GameManager.Instance.categoryInfosColors[3];
            }
            else if (categoryInfo.colorType == CategoryInfo.ColorType.PURPLE)
            {
                backgroundImage.sprite = GameManager.Instance.categoryInfoSpries[4];
                nameText.color = GameManager.Instance.categoryInfosColors[4];
            }

            SetProgress(categoryInfo);

            SetLocked(categoryInfo);
        }

        public override void Removed()
        {
#if BBG_IAP
			waitingForIAPInitialize					= false;
			IAPManager.Instance.OnIAPInitialized	-= OnIAPInitialized;
#endif
        }

        #endregion

        #region Private Methods

        private void SetProgress(CategoryInfo categoryInfo)
        {
            int totalLevels = categoryInfo.levelFiles.Count;
            int numLevelsCompleted = GameManager.Instance.LastCompletedLevels.ContainsKey(categoryInfo.saveId) ? GameManager.Instance.LastCompletedLevels[categoryInfo.saveId] + 1 : 0;

            levelProgressBar.SetProgress((float)numLevelsCompleted / (float)totalLevels);

            levelProgressText.text = string.Format("{0} / {1}", numLevelsCompleted, totalLevels);
        }

        private void SetLocked(CategoryInfo categoryInfo)
        {
            bool isCategoryLocked = GameManager.Instance.IsCategoryLocked(categoryInfo);

            progressBarContainer.SetActive(!isCategoryLocked);
            lockedContainer.SetActive(isCategoryLocked);
            //coinsUnlockContainer.SetActive(isCategoryLocked && categoryInfo.lockType == CategoryInfo.LockType.Coins);
            //keysUnlockContainer.SetActive(isCategoryLocked && categoryInfo.lockType == CategoryInfo.LockType.Keys);
            //iapUnlockContainer.SetActive(isCategoryLocked && categoryInfo.lockType == CategoryInfo.LockType.IAP);

#if BBG_IAP
			waitingForIAPInitialize					= false;
			IAPManager.Instance.OnIAPInitialized	-= OnIAPInitialized;
#endif

            switch (categoryInfo.lockType)
            {
                case CategoryInfo.LockType.Coins:
                    coinsUnlockAmountText.text = "" + categoryInfo.unlockAmount;
                    break;
                //case CategoryInfo.LockType.Keys:
                //	keysUnlockAmountText.text = "" + categoryInfo.unlockAmount;
                //	break;
                case CategoryInfo.LockType.IAP:
                    SetIAPPrice(categoryInfo.iapProductId);
                    break;
            }
        }

        private void SetIAPPrice(string productId)
        {
#if BBG_IAP
			if (IAPManager.Instance.IsInitialized)
			{
				UnityEngine.Purchasing.Product product = IAPManager.Instance.GetProductInformation(productId);

				if (product == null)
				{
					iapUnlockPriceText.text = "Does Not Exist";
				}
				else
				{
					iapUnlockPriceText.text = product.metadata.localizedPriceString;
				}
			}
			else
			{
				lockedProductId							= productId;
				waitingForIAPInitialize					= true;
				IAPManager.Instance.OnIAPInitialized	+= OnIAPInitialized;
			}
#else
            iapUnlockPriceText.text = "IAP Not Enabled";
#endif
        }

        private void OnIAPInitialized(bool success)
        {
#if BBG_IAP
			waitingForIAPInitialize					= false;
			IAPManager.Instance.OnIAPInitialized	-= OnIAPInitialized;

			if (success)
			{
				SetIAPPrice(lockedProductId);
			}
			else
			{
				iapUnlockPriceText.text = "Failed To Initialize";
			}
#endif
        }

        #endregion
    }
}
