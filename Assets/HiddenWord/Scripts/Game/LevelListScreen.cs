﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace dotmob.HiddenWord
{
	public class LevelListScreen : Screen
	{
		#region Inspector Variables

		[Space]

		[SerializeField] private LevelListItem	levelListItemPrefab	= null;
		[SerializeField] private RectTransform	levelListContainer	= null;
		[SerializeField] private ScrollRect		levelListScrollRect	= null;

		#endregion

		#region Member Variables

		private RecyclableListHandler<int> levelListHandler;

		#endregion

		#region Public Methods

		public override void Show(bool back, bool immediate)
		{
			base.Show(back, immediate);

			if (back)
			{
				levelListHandler.Refresh();
					Debug.Log("Level list nll");

			}
			else
			{
				if (levelListHandler == null)
				{
					Debug.Log("Level list nll");
					List<int> levelIndicies = new List<int>();

					for (int i = 0; i < GameManager.Instance.ActiveCategoryInfo.levelFiles.Count; i++)
					{
						levelIndicies.Add(i);
					}

					levelListHandler					= new RecyclableListHandler<int>(levelIndicies, levelListItemPrefab, levelListContainer, levelListScrollRect);
					levelListHandler.OnListItemClicked	= OnLevelListItemClicked;
					levelListHandler.Setup();
				}
				else
				{
					levelListHandler.Refresh();
				}
			}
		}

		#endregion

		#region Private Methods

		private void OnLevelListItemClicked(int levelIndex)
		{
			if (!GameManager.Instance.IsLevelLocked(GameManager.Instance.ActiveCategoryInfo, levelIndex))
			{

				GameManager.Instance.gameType = GameType.NORMAL;
				GameManager.Instance.levelInd = levelIndex;
				GameManager.Instance.StartLevel(GameManager.Instance.ActiveCategoryInfo, levelIndex);
			}
			
		}

		#endregion
	}
}
