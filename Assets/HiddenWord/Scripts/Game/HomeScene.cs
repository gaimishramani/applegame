﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace dotmob.HiddenWord
{
    public class HomeScene : Screen
    {
        private void Start()
        {
            SoundManager.Instance.Play("enter-game");
            Invoke("InvokeGamePlay", 2f);
        }

        public void InvokeGamePlay()
        {
            ScreenManager.Instance.Show("main");
            //GameManager.Instance.bottomBar.levelButton.color = GameManager.Instance.selectedColor;
            //GameManager.Instance.bottomBar.dailyChallangeButton.color = Color.white;
            SoundManager.Instance.StartLoopMusic();
        }

    }
}