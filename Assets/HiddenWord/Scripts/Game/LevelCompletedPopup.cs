﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace dotmob.HiddenWord
{
    public class LevelCompletedPopup : Popup
    {
        #region Inspector Variables

        [Space]

        [SerializeField] private GameObject playAgainButton = null;
        [SerializeField] private GameObject nextLevelButton = null;

        [Space]

        [SerializeField] private GameObject rewardsContainer = null;
        [SerializeField] private GameObject coinRewardContainer = null;
        //[SerializeField] private GameObject keyRewardContainer		= null;
        [SerializeField] private Text coinRewardAmountText = null;
        //	[SerializeField] private Text		keyRewardAmountText		= null;

        public Text titleText;
        public TextMeshProUGUI completedLevelText;
        public TextMeshProUGUI totalLevelText;
        public Image progresBarImage;

        #endregion

        #region Public Methods

        public override void OnShowing(object[] inData)
        {
            base.OnShowing(inData);
            AdsManager.Instance.ShowInterstitial();
            SoundManager.Instance.Play("level-complete");
            bool progressLevelCompleted = (bool)inData[0];
            int coinsAwarded = (int)inData[1];
            int keyAwarded = (int)inData[2];
            bool lastLevel = (bool)inData[3];

            playAgainButton.SetActive(!progressLevelCompleted);
            nextLevelButton.SetActive(progressLevelCompleted && !lastLevel);

            bool awardCoins = coinsAwarded > 0;
            bool awardKeys = keyAwarded > 0;
            SoundManager.Instance.Play("coin-collect");
            //rewardsContainer.SetActive(awardCoins || awardKeys);
            //coinRewardContainer.SetActive(awardCoins);
            //keyRewardContainer.SetActive(awardKeys);

            //coinRewardAmountText.text	= "x " + coinsAwarded;
            //			keyRewardAmountText.text	= "x " + keyAwarded;
        }

        public void SetLevelCompletedData(int completedLevel, int totalLevel)
        {
            titleText.text = "Level " + " Completed! " + completedLevel;
            completedLevelText.text = completedLevel.ToString();
            totalLevelText.text = totalLevel.ToString();
            progresBarImage.fillAmount = (1 / (float)totalLevel) * completedLevel;
        }
        #endregion
    }
}
