﻿using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace dotmob
{
    public class SwitchToggle : TButton
    {
        public SoundType soundType;
        [SerializeField] RectTransform uiHandleRectTransform;
        [SerializeField] Color backgroundActiveColor;
        [SerializeField] Color handleActiveColor;

        public Sprite redBackgroundImage;
        public Sprite redHandleImage;

        public Sprite greenBackgroundImage;
        public Sprite greenHandleImage;

        Image backgroundImage, handleImage;
        public GameObject ON;
        public GameObject OFF;

        Color backgroundDefaultColor, handleDefaultColor;

        Toggle toggle;

        Vector2 handlePosition;
        public SelectableType selectableType;

        void Awake()
        {
            toggle = GetComponent<Toggle>();

            handlePosition = uiHandleRectTransform.anchoredPosition;

            backgroundImage = uiHandleRectTransform.parent.GetComponent<Image>();
            handleImage = uiHandleRectTransform.GetComponent<Image>();

            backgroundDefaultColor = backgroundImage.color;
            handleDefaultColor = handleImage.color;

            toggle.onValueChanged.AddListener(OnSwitch);
            if (toggle.isOn)
                OnSwitch(true);

            selectableType = SelectableType.OFF;
            //OnSwitch(true);
        }


        void OnSwitch(bool on)
        {
            //uiHandleRectTransform.anchoredPosition = on ? handlePosition * -1 : handlePosition ; // no anim
            uiHandleRectTransform.DOAnchorPos(on ? handlePosition * -1 : handlePosition, .4f).SetEase(Ease.InOutBack);

            /*//backgroundImage.color = on ? backgroundActiveColor : backgroundDefaultColor ; // no anim
            backgroundImage.DOColor (on ? backgroundActiveColor : backgroundDefaultColor, .6f) ;

            //handleImage.color = on ? handleActiveColor : handleDefaultColor ; // no anim
            handleImage.DOColor (on ? handleActiveColor : handleDefaultColor, .4f) ;*/

            /*if (selectableType == SelectableType.ON)
            {
                selectableType = SelectableType.OFF;
                backgroundImage.sprite = redBackgroundImage;
                handleImage.sprite = redHandleImage;

                *//* if (soundType == SoundType.Music)
                 {
                     SoundManager.Instance.StopMusic();
                 }
                 else if (soundType == SoundType.Sound)

                 {
                     SoundManager.Instance.StopSound();
                 }*//*
            }
            else if (selectableType == SelectableType.OFF)
            {
                selectableType = SelectableType.ON;
                backgroundImage.sprite = greenBackgroundImage;
                handleImage.sprite = greenHandleImage;

                *//*if (soundType == SoundType.Music)
                {
                    Debug.Log("music bandh  ma aavyu ");
                    PlayerPrefs.SetInt("isMusicOn", 1);
                    SoundManager.Instance.isMusicOn = PlayerPrefs.GetInt("isMusicOn", 1);
                }
                else if (soundType == SoundType.Sound)
                {
                    Debug.Log("sound bandh ma aavyu ");
                    PlayerPrefs.SetInt("isSoundOn", 1);
                    SoundManager.Instance.hello = PlayerPrefs.GetInt("isSoundOn", 1);
                }*//*
            }*/
            if (soundType == SoundType.Music)
            {
                base.OnButtonClick();
                //Music.instance.SetEnabled(IsOn, true);
                SoundManager.Instance.SetSoundTypeOnOff(SoundManager.SoundType.Music, isOn);
                

            }
            if (soundType == SoundType.Sound)
            {
                base.OnButtonClick();
                SoundManager.Instance.SetSoundTypeOnOff(SoundManager.SoundType.SoundEffect, isOn);
            }
            if (soundType == SoundType.Vibration)
            {
                base.OnButtonClick();
                SoundManager.Instance.isVibration = isOn;
            }
            else if(soundType == SoundType.None)
            {
                isOn = !isOn;
            }
            if (!isOn)
            {
                ON.SetActive(false);
                OFF.SetActive(true);
                backgroundImage.sprite = redBackgroundImage;
                handleImage.sprite = redHandleImage;
                
            }
            else
            {
                ON.SetActive(true);
                OFF.SetActive(false);
                backgroundImage.sprite = greenBackgroundImage;
                handleImage.sprite = greenHandleImage;
                
            }

        }

        void OnDestroy()
        {
            toggle.onValueChanged.RemoveListener(OnSwitch);
        }

        public enum SelectableType
        {
            ON,
            OFF
        }
    }
}