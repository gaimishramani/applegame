using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleMobileAds.Api;

namespace dotmob.HiddenWord
{
    public class AdsManager : MonoBehaviour
    {
        public GameObject rewardAdLabel;
        public GameObject rewardAdNotAvail;
        public GameObject adBtn;
        public Text waitingText;
        public string lastRewardDateString = "";
        public static AdsManager Instance;
        string InterstitialAdID;
        string RewardedAdID;
        public bool ShowAds = true;

        //  string BannerAdID;

        bool GrandFreeCoins = false;
        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(Instance);
            }
            else
            {
                Destroy(gameObject);
            }

            // if (Application.platform == RuntimePlatform.Android)
            {
                #region TestId
                //BannerAdID = "ca-app-pub-3940256099942544/6300978111";
                InterstitialAdID = "ca-app-pub-9184061326644149/9042064035";
                RewardedAdID = "ca-app-pub-9184061326644149/8906332664";
                #endregion

                #region Real Ad Id
                //BannerAdID = "ca-app-pub-3794555076315548/9353557244";
                // InterstitialAdID = "ca-app-pub-5950171287202667/3985215187";
                //RewardedAdID = "ca-app-pub-5950171287202667/9849530357";
                #endregion
            }
            if (PlayerPrefs.HasKey("NoAds"))
            {
                ShowAds = false;
            }
        }
        private void Start()
        {  // Initialize the Google Mobile Ads SDK.
            MobileAds.Initialize(initStatus => { });
            LoadAds();
        }
        private void Update()
        {
            if (GrandFreeCoins)
            {
                GrandFreeCoins = false;
                //  UIManagerNew.Instance.HintView.UseEarnCoins();
                // MainMenuManager.Instance.earnCoins.GiveVideoAdReward();
            }
        }
        private void OnDisable()
        {
            StopCoroutine(nameof(CheckRewardStatusCoroutine));
        }
        public void CheckLastRewardDate()
        {
            adBtn.SetActive(false);
            rewardAdLabel.SetActive(false);
            rewardAdNotAvail.SetActive(false);
            waitingText.gameObject.SetActive(false);
            lastRewardDateString = PlayerPrefs.GetString("LastRewardKey", DateTime.Now.AddDays(-2).ToString());
            // Parse the stored string into a DateTime object
            DateTime lastRewardDate;
            if (DateTime.TryParse(lastRewardDateString, out lastRewardDate))
            {
                // Get the current time
                DateTime currentTime = DateTime.Now;

                // Calculate the time difference
                TimeSpan timeDifference = currentTime - lastRewardDate;

                // Check if the difference is 6 hours or more
                if (timeDifference.TotalHours >= 6)
                {
                    Debug.Log("More than 6 hours have passed since the last reward.");
                    // Reward the player or perform the necessary action
                    if (rewardedAdEarnCoins != null && rewardedAdEarnCoins.CanShowAd())
                    {
                        adBtn.SetActive(true);
                        rewardAdLabel.SetActive(true);
                        rewardAdNotAvail.SetActive(false);
                        waitingText.gameObject.SetActive(false);
                        StopCoroutine(nameof(CheckRewardStatusCoroutine));
                    }
                    else
                    {
                        adBtn.SetActive(false);
                        rewardAdLabel.SetActive(false);
                        rewardAdNotAvail.SetActive(true);
                        waitingText.gameObject.SetActive(false);
                        StopCoroutine(nameof(CheckRewardStatusCoroutine));
                    }
                }
                else
                {
                    Debug.Log("Less than 6 hours since the last reward.");
                    adBtn.SetActive(false);
                    rewardAdLabel.SetActive(false);
                    rewardAdNotAvail.SetActive(false);
                    StartCoroutine(nameof(CheckRewardStatusCoroutine));
                }
            }
            else
            {
                Debug.LogError("Failed to parse lastRewardDateString.");
            }
        }
        IEnumerator CheckRewardStatusCoroutine()
        {
            waitingText.gameObject.SetActive(true);
            while (true)
            {
                CheckRewardStatus();
                yield return new WaitForSeconds(1); // Check every second
                Debug.Log("CheckRewardStatusCoroutine called");
            }
            waitingText.gameObject.SetActive(false);
        }
        void CheckRewardStatus()
        {
            DateTime lastRewardDate;
            if (DateTime.TryParse(lastRewardDateString, out lastRewardDate))
            {
                DateTime currentTime = DateTime.Now;
                TimeSpan timeDifference = currentTime - lastRewardDate;
                TimeSpan rewardInterval = TimeSpan.FromHours(6);

                if (timeDifference >= rewardInterval)
                {
                    waitingText.text = "You can claim your reward now!";
                }
                else
                {
                    TimeSpan remainingTime = rewardInterval - timeDifference;
                    waitingText.text = string.Format("Next reward in: {0:D2}:{1:D2}:{2:D2}",
                                                    remainingTime.Hours,
                                                    remainingTime.Minutes,
                                                    remainingTime.Seconds);
                    Debug.Log("CheckRewardStatusCoroutine remainingTime " + remainingTime.Seconds);

                }
            }
        }
        public void LoadAds()
        {
            if (rewardedAdEarnCoins == null || !rewardedAdEarnCoins.CanShowAd())
            {
                RequestEarnCoinsRewardedAd();
            }

            if (!ShowAds)
            {
                return;
            }
            if (interstitial == null || !interstitial.CanShowAd())
            {
                RequestInterstitial();
            }

        }
        public void RemoveAds()
        {
            ShowAds = false;
            PlayerPrefs.SetInt("NoAds", 1);
            //UIManagerNew.Instance.HomeView.DisableRemoveAdsUI();

        }

        #region Banner Ads

        //private BannerView bannerView;
        //bool isBannerAdLoaded = false;

        //private void RequestBanner()
        //{
        //    int xSize = Screen.width - 20;
        //    int ySize = (int)(Screen.height * .08f);
        //    //AdSize aSize = new AdSize(300, ySize);
        //    AdSize aSize = new AdSize(728, 32);
        //    ////////////////Debug.Log("REQBanner");
        //    this.bannerView = new BannerView(BannerAdID, aSize, AdPosition.Bottom);

        //    // Called when an ad request has successfully loaded.
        //    this.bannerView.OnBannerAdLoaded += this.BannerAdLoaded;
        //    // Called when an ad request failed to load.
        //    this.bannerView.OnBannerAdLoadFailed += this.BannerAdFailedToLoad;

        //    // Create an empty ad request.
        //    AdRequest request = new AdRequest.Builder().Build();

        //    // Load the banner with the request.
        //    this.bannerView.LoadAd(request);

        //}
        //public void ShowBanner()
        //{
        //    if (!ShowAds)
        //    {
        //        return;
        //    }
        //    ////////////////Debug.Log("ShowBanner");
        //    if (!isBannerAdLoaded)
        //    {
        //        RequestBanner();
        //        return;
        //    }
        //    bannerView.Show();
        //}
        //public void HideBanner()
        //{
        //    if (!isBannerAdLoaded)
        //    {
        //        return;
        //    }
        //    ////////////////Debug.Log("HideBanner");
        //    if (isBannerAdLoaded)
        //        bannerView.Hide();
        //}

        //public void DestroyBanner()
        //{
        //    this.bannerView.Destroy();
        //    isBannerAdLoaded = false;
        //}

        //public void BannerAdLoaded()
        //{
        //    isBannerAdLoaded = true;        
        //    if (MainMenuManager.Instance == null)
        //    {
        //        HideBanner();
        //    }
        //}

        //public void BannerAdFailedToLoad(LoadAdError error)
        //{
        //    isBannerAdLoaded = false;
        //}

        #endregion

        #region Interstitial Ads

        private InterstitialAd interstitial;

        private void RequestInterstitial()
        {
            if (interstitial != null)
            {
                interstitial.Destroy();
                interstitial = null;
            }

            AdRequest request = new AdRequest();
            InterstitialAd.Load(InterstitialAdID, request, ProcessIntertitialRequest);
        }
        protected void ProcessIntertitialRequest(InterstitialAd ad, LoadAdError error)
        {
            // if error is not null, the load request failed.
            if (error != null || ad == null)
            {
                ////////////////Debug.LogError("interstitial ad failed to load an ad " + "with error : " + error);
                return;
            }

            ////////////////Debug.Log("Interstitial ad loaded with response : " + ad.GetResponseInfo());
            ad.OnAdFullScreenContentClosed += HandleOnAdClosed;

            interstitial = ad;
        }
        public void ShowInterstitial()
        {
            /*if (!ShowAds)
            {
                return;
            }
            if (interstitial != null && interstitial.CanShowAd())
            {
                ////////////////Debug.Log("Showing interstitial ad.");
                interstitial.Show();
            }
            else
            {
                RequestInterstitial();
            }*/
        }

        public void HandleOnAdClosed()
        {
            ////////////////Debug.Log("Ad Closed");
            interstitial.Destroy();
            RequestInterstitial();
        }
        #endregion

        #region Rewarded Ads For Hint

        public RewardedAd rewardedAdEarnCoins = null;
        public void RequestEarnCoinsRewardedAd()
        {
            if (rewardedAdEarnCoins != null)
            {
                rewardedAdEarnCoins.Destroy();
                rewardedAdEarnCoins = null;
            }

            AdRequest request = new AdRequest();
            RewardedAd.Load(RewardedAdID, request, ProcessRewardRequestEarnCoins);
        }

        void ProcessRewardRequestEarnCoins(RewardedAd ad, LoadAdError error)
        {
            // if error is not null, the load request failed.
            if (error != null || ad == null)
            {
                ////////////////Debug.LogError("Rewarded ad failed to load an ad " + "with error : " + error);
                return;
            }

            ad.OnAdFullScreenContentClosed += HandleRewardedAdEarnCoinsClosed;
            ////////////////Debug.Log("Rewarded ad loaded with response : " + ad.GetResponseInfo());
            rewardedAdEarnCoins = ad;
            adBtn.SetActive(true);
            rewardAdLabel.SetActive(true);
            rewardAdNotAvail.SetActive(false);
            waitingText.gameObject.SetActive(true);
            StopCoroutine(nameof(CheckRewardStatusCoroutine));
        }

        // bool IsRewardAdLoaded = false;
        public void ShowRewardedAdEarnCoins()
        {
            if (rewardedAdEarnCoins != null && rewardedAdEarnCoins.CanShowAd())
            {
                // IsRewardAdLoaded = true;
                rewardedAdEarnCoins.Show(RewardPlayerEarnCoins);
                StopCoroutine(nameof(CheckRewardStatusCoroutine));
            }
            else
            {
                // IsRewardAdLoaded = false;
                adBtn.SetActive(false);
                rewardAdLabel.SetActive(false);
                rewardAdNotAvail.SetActive(true);
                RequestEarnCoinsRewardedAd();
            }
        }
        void RewardPlayerEarnCoins(Reward reward)
        {
            ////////////////Debug.Log("Reward Player");
            GrandFreeCoins = true;
            GameManager.Instance.GiveCoins(50);
            SoundManager.Instance.Play("coin-collect");
            PopupManager.Instance.rewardAdPopup.Hide(true);
            PlayerPrefs.SetString("LastRewardKey", DateTime.Now.ToString());
        }
        public void HandleRewardedAdEarnCoinsClosed()
        {
            ////////////////Debug.Log("RClosed");
            rewardedAdEarnCoins.Destroy();
            RequestEarnCoinsRewardedAd();
        }

        #endregion
    }
}