﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CategoryInfo
{
	#region Enums

	public enum LockType
	{
		None,
		Coins,
		IAP
	}
	public enum ColorType
    {
		PINK,
		GREEN,
		CYAN,
		ORANGE,
		PURPLE
    }

	#endregion

	public string			displayName;
	public string			saveId;
	public Sprite			icon;
	public TextAsset		wordFile;
	public List<TextAsset>	levelFiles;
	public LockType			lockType;
	public int				unlockAmount;
	public string			iapProductId;
	public ColorType colorType;
}
