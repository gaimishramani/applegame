﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace dotmob.HiddenWord
{
    [RequireComponent(typeof(Button))]
    public class ButtonManager : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        private TextMeshProUGUI label;

        private Button button;
        private UnityAction buttonAction;

        #endregion

        #region Public Methods

        public void Initialize(string label, Action<(string, string)> clickEventHandler)
        {
            this.label.text = label;
            if (CalendarManager.Instance.GetCurrentDate().ToString() == label)
            {
                Debug.LogError("month start day" + CalendarManager.Instance.GetCurrentDate().ToString());
                SetCurrentDate();
            }

            //Debug.LogError("has key "+label + CalendarManager.Instance.currentMonth + CalendarManager.Instance.currentYear);
            DoColor();
            buttonAction += () => clickEventHandler((label, label));
            button.onClick.AddListener(buttonAction);
        }
        public void DoColor()
        {
            if (PlayerPrefs.HasKey(label.text + CalendarManager.Instance.currentMonth.ToString() + CalendarManager.Instance.currentYear.ToString()))
            {
                Debug.LogError("HAS KEY" + label.text + CalendarManager.Instance.currentMonth.ToString() + CalendarManager.Instance.currentYear.ToString());
                string val = PlayerPrefs.GetString(label.text + CalendarManager.Instance.currentMonth.ToString() + CalendarManager.Instance.currentYear.ToString());
                this.gameObject.GetComponent<Image>().enabled = true;
                if (val == "bronze")
                {
                    this.gameObject.GetComponent<Image>().sprite = CalendarManager.Instance.trophyImage.sprite;
                    label.alpha = 0;
                }
                else if (val == "silver")
                {
                    this.gameObject.GetComponent<Image>().sprite = CalendarManager.Instance.trophyImage.sprite;
                    label.alpha = 0;
                }
                else if (val == "gold")
                {
                    this.gameObject.GetComponent<Image>().sprite = CalendarManager.Instance.trophyImage.sprite;
                    label.alpha = 0;
                }
                if (CalendarManager.Instance.GetCurrentDate().ToString() == DateTime.Now.Date.ToString())
                {
                    Debug.LogError("month start day" + CalendarManager.Instance.GetCurrentDate().ToString());
                    SetCurrentDate();
                }
            }
        }
        public void SetCurrentDate()
        {
            this.gameObject.GetComponent<Image>().enabled = true;
            this.gameObject.GetComponent<Image>().color = Color.cyan;
        }
        #endregion

        #region Private Methods

        private void Awake()
        {
            button = GetComponent<Button>();
        }

        private void OnDestroy()
        {
            button.onClick.RemoveListener(buttonAction);
        }

        #endregion
    }
}