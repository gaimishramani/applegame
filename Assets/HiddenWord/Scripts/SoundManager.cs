using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    public Sound soundClip;

    public static SoundManager Instance;

    public int isMusicOn;
    //public int isSoundOn;
    public int hello;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        foreach (SoundData s in soundClip.soundClips)
        {
            s.audioSource = gameObject.AddComponent<AudioSource>();
            s.audioSource.clip = s.clip;
            s.audioSource.loop = s.isLoop;
            s.audioSource.volume = s.volume;
            s.audioSource.pitch = s.pitch;
        }
        isMusicOn = PlayerPrefs.GetInt("isMusicOn", 1);
        Debug.Log("music "+isMusicOn);
        hello = PlayerPrefs.GetInt("isSoundOn", 1);
        Debug.Log("sound "+hello);

    }

    private void Start()
    {
       
        PlaySound("BG");
        Debug.Log("bg vaagado  ");

    }

    public void PlaySound(string name)
    {
        SoundData s = Array.Find(soundClip.soundClips, soundClip => soundClip.soundName == name);
        if ((s.soundType == SoundType.Music && isMusicOn == 0) || s.soundType == SoundType.Sound && hello == 0)
        {
            return;
        }

        s.audioSource.Play();
    }

    public void StopSound(string name)
    {
        SoundData s = Array.Find(soundClip.soundClips, soundClip => soundClip.soundName == name);
        s.audioSource.Stop();
    }

    public void StopMusic()
    {
        PlayerPrefs.SetInt("isMusicOn", 0);
        isMusicOn = PlayerPrefs.GetInt("isMusicOn", 0);
        Debug.Log("Stop MUsic"+isMusicOn);
        foreach (SoundData sd in soundClip.soundClips)
        {
            if (sd.soundType == SoundType.Music)
            {
                sd.audioSource.Stop();
            }
        }
    }

    public void StopSound()
    {
        PlayerPrefs.SetInt("isSoundOn", 0);
        hello= PlayerPrefs.GetInt("isSoundOn", 0);
        Debug.Log("Stop sound"+hello);

        foreach (SoundData sd in soundClip.soundClips)
        {
            if (sd.soundType == SoundType.Sound)
            {
                sd.audioSource.Stop();
            }
        }
    }
}