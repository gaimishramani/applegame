﻿using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;


namespace dotmob.HiddenWord
{
    public class CalendarManager : MonoBehaviour
    {
        #region Fields
        public Image trophyImage;
        public GameObject prevMonthButton;
        public GameObject nextMonthButton;

        public GameObject playBtton;
        [SerializeField]
        private HeaderManager headerManager;

        [SerializeField]
        private BodyManager bodyManager;

        [SerializeField]
        private TailManager tailManager;

        private DateTime targetDateTime;
        private CultureInfo cultureInfo;

        public static CalendarManager Instance;

        public string currentMonth;
        public string currentMonthInWord;
        public string currentYear;
        public int startDay;
        public int endDay;

        public DailyChallangeScreen dailyChallangeScreen;
        #endregion

        #region Public Methods

        public void OnGoToPreviousOrNextMonthButtonClicked(string param)
        {
            targetDateTime = targetDateTime.AddMonths(param == "Prev" ? -1 : 1);
            EnableOrDisableNextMonthButton();
            EnableOrDisablePrevMonthButton();
            Refresh(targetDateTime.Year, targetDateTime.Month);
            SoundManager.Instance.Play("btn-click");
        }

        public void EnableOrDisableNextMonthButton()
        {
            if (targetDateTime.Month == DateTime.Now.Month)
            {
                nextMonthButton.SetActive(false);
                playBtton.SetActive(true);

            }
            else
            {
                playBtton.SetActive(false);
                nextMonthButton.SetActive(true);
            }
        }
        public void EnableOrDisablePrevMonthButton()
        {
            if (targetDateTime.Month == (DateTime.Now.Month - 2))
            {
                prevMonthButton.SetActive(false);
            }
            else
            {
                prevMonthButton.SetActive(true);
            }
        }
        #endregion

        #region Private Methods

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        private void Start()
        {
            Debug.LogError(PlayerPrefs.GetString("September2022"));
            Debug.LogError(PlayerPrefs.GetInt(("Completed" + PlayerPrefs.GetString("September2022"))));

            Debug.LogError(PlayerPrefs.GetString("August2022"));
            Debug.LogError(PlayerPrefs.GetInt(("Completed" + PlayerPrefs.GetString("August2022"))));

            Debug.LogError(PlayerPrefs.GetString("July2022"));
            Debug.LogError(PlayerPrefs.GetInt(("Completed" + PlayerPrefs.GetString("July2022"))));

            /*PlayerPrefs.SetString("September2022", "September2022");
            PlayerPrefs.SetInt(("Completed" + PlayerPrefs.GetString("September2022")),25);

            PlayerPrefs.SetString("August2022", "August2022");
            PlayerPrefs.SetInt(("Completed" + PlayerPrefs.GetString("August2022")), 15);

            PlayerPrefs.SetString("July2022", "July2022");
            PlayerPrefs.SetInt(("Completed" + PlayerPrefs.GetString("July2022")),10);*/

            Debug.Log("set");
            targetDateTime = DateTime.Now;
            cultureInfo = new CultureInfo("en-US");
            Refresh(targetDateTime.Year, targetDateTime.Month);
            EnableOrDisableNextMonthButton();

        }


        #endregion
        public void SetDailyChallangeLevelPref()
        {
            int crrentDate = GetCurrentDate();
            string val1 = crrentDate + currentMonth + currentYear;
            PlayerPrefs.SetString(val1, "bronze");
            Debug.LogError("pref set " + val1);
            int val2 = PlayerPrefs.GetInt(("Completed" + PlayerPrefs.GetString(currentMonthInWord + currentYear.ToString())));
            PlayerPrefs.SetInt(("Completed" + PlayerPrefs.GetString(currentMonthInWord + currentYear.ToString())), (val2 + 1));
            Debug.LogError("set total days" + PlayerPrefs.GetInt(("Completed" + PlayerPrefs.GetString(currentMonthInWord + currentYear.ToString()))));
            dailyChallangeScreen.SetProgressBarData();


        }
        public void RefreshLevelPref()
        {
            for (int i = 0; i <= bodyManager.cells.Count; i++)
            {
                if (bodyManager.cells[i].GetComponent<ButtonManager>())
                    bodyManager.cells[i].GetComponent<ButtonManager>().DoColor();
            }
        }
        #region Event Handlers

        private void Refresh(int year, int month)
        {
            currentMonthInWord = cultureInfo.DateTimeFormat.GetMonthName(month);
            headerManager.SetTitle($"{year} {cultureInfo.DateTimeFormat.GetMonthName(month)}");

            currentMonth = month.ToString();
            currentYear = year.ToString();
            bodyManager.Initialize(year, month, OnButtonClicked);

            String key = currentMonthInWord + year.ToString();
            if (PlayerPrefs.HasKey(key))
            {
                Debug.LogError("has key");
                dailyChallangeScreen.SetProgressBarData();
            }
            else
            {
                ResetMonthAndYear(month, year);
            }
        }
        private void ResetMonthAndYear(int month, int year)
        {
            String key = currentMonthInWord + year.ToString();
            PlayerPrefs.SetString(key, key);
            Debug.Log("CurrentMonthYear not set" + PlayerPrefs.GetString(key));
            //int totalDailyChallangeLevels = GetTotalNumberOfDays(year, month);
            //PlayerPrefs.SetString("TotalDailyChallangeLevels", totalDailyChallangeLevels.ToString());
            //Debug.Log("TotalDailyChallangeLevels" + PlayerPrefs.GetString("TotalDailyChallangeLevels"));
            PlayerPrefs.SetInt(("Completed" + key.ToString()), 0);
            dailyChallangeScreen.SetProgressBarData();
        }
        private void OnButtonClicked((string day, string legend) param)
        {
            tailManager.SetLegend($"You have clicked day {param.day}.");
        }
        public int GetCurrentDate()
        {
            startDay = 1;
            endDay = GetTotalNumberOfDays(targetDateTime.Year, targetDateTime.Month);
            if (DateTime.Now.Year == targetDateTime.Year && DateTime.Now.Month == targetDateTime.Month)
            {
                return (DateTime.Now.Day - 1) + startDay;
            }

            return 0;
        }
        int GetMonthStartDay(int year, int month)
        {
            DateTime temp = new DateTime(year, month, 1);

            //DayOfWeek Sunday == 0, Saturday == 6 etc.
            return (int)temp.DayOfWeek;
        }
        public int GetTotalNumberOfDays(int year, int month)
        {
            return DateTime.DaysInMonth(year, month);
        }
        #endregion
    }
}